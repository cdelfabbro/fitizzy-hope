<!-- Topic Header -->
<div class="topic">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h3 class="primary-font">Fitizzy-hope</h3>
      </div>
      <div class="col-sm-8">
        <ol class="breadcrumb pull-right hidden-xs">
          <li><a href="<?php echo site_url("");?>">Liste des utilisateurs</a></li>
          <li class="active">Modifier un utilisateur</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="sign-form">
        <h3 class="first-child">Modifier un utilisateur</h3>
        <hr>
        <form role="form" method="post" action="<?php echo site_url('users/update'); ?>">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h5>Le compte</h5>
              <div class="form-group">
                <input type="hidden" name="id" value="<?= $user['id'] ?>">
                <input type="email" class="form-control" id="email" name="email" placeholder="Adresse email" value="<?= $user['email'] ?>" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez une adresse email valide ici." data-original-title="Adresse email">
                <span class="text-danger"><?php echo form_error('email'); ?></span>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="editPassword" name="editPassword" value="checked"> Je veux modifier le mot de passe
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control margin-bottom-xs" id="old-password" name="old-password" placeholder="Ancien mot de passe" required="required" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="Entrez votre ancien mot de passe" data-original-title="Ancien mot de passe" disabled="">
                    <span class="text-danger"><?php echo form_error('old-password'); ?></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <input type="password" class="form-control margin-bottom-xs" id="password" name="password" placeholder="Nouveau mot de passe" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez votre nouveau mot de passe. Cinq caractères minimum." data-original-title="Nouveau mot de passe" disabled="">
                    <span class="text-danger"><?php echo form_error('password'); ?></span>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control" id="repeat-password" name="repeat-password" placeholder="Répétez le mot de passe" required="required" data-toggle="popover" data-trigger="focus" data-content="Être sûr de toujours s'en rappeler." data-original-title="Répétez le mot de passe" disabled="">
                    <span class="text-danger"><?php echo form_error('repeat-password'); ?></span>
                  </div>
                </div>
              </div>
              <hr>

              <h5>Le profil</h5>
              <div class="form-group">
                <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" value="<?= $user['firstname'] ?>" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez votre prénom ici." data-original-title="Prénom">
                <span class="text-danger"><?php echo form_error('prenom'); ?></span>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" value="<?= $user['lastname'] ?>" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez votre nom ici." data-original-title="Nom">
                <span class="text-danger"><?php echo form_error('nom'); ?></span>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-primary">Modifier l'utilisateur</button><hr>
                <span class="error text-danger"><?php if (isset($error)) { echo $error; } ?></span>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div> <!-- / .row -->
</div> <!-- / container -->
