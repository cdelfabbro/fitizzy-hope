<!-- Topic Header -->
<div class="topic">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h3 class="primary-font">Fitizzy-hope</h3>
      </div>
      <div class="col-sm-8">
        <ol class="breadcrumb pull-right hidden-xs">
          <li><a href="<?php echo site_url("");?>">Liste des utilisateurs</a></li>
          <li class="active">Voir mon profil</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="jumbotron text-center">
    <?php if ($this->session->userdata('user_id_fb')) { # user connecté par FB ?>
      <img class="img-thumbnail" data-src="holder.js/140x140" alt="140x140" src="https://graph.facebook.com/<?=$this->session->userdata('user_id_fb')?>/picture?type=large" style="width: 140px; height: 140px;">
    <?php } ?>
    <h2><?php echo $user['firstname'].' '.$user['lastname']; ?></h2><hr>
    <div class="container">
      <p><strong>Email: </strong><?= $user['email'] ?></p>
      <p><strong>Prénom: </strong><?= $user['firstname'] ?></p>
      <p><strong>Nom: </strong><?= $user['lastname'] ?></p>
      <p><strong>Date d'inscription: </strong><?= $user['created_at'] ?></p>
      <p><strong>Type de compte: </strong><?php echo ($user['fb_account'] == '1') ? 'Facebook' : 'Email' ?></p><hr>
      <?php if (!$this->session->userdata('user_id_fb')) { # user connecté par FB ?>
        <a href="<?php echo site_url('users/edit?id='.$user['id'].'&amp;email='.$user['email']); ?>" class="btn btn-primary">Modifier mon compte</a>
      <?php } else { ?>
        <a href="<?php echo $this->session->userdata('user_link'); ?>" class="btn btn-primary">Voir mon profil sur Facebook</a>
      <?php } ?>
    </div>
  </div>
</div> <!-- / .container -->
