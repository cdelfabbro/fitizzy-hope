<!-- Topic Header -->
<div class="topic">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h3 class="primary-font">Fitizzy-hope</h3>
      </div>
      <div class="col-sm-8">
        <ol class="breadcrumb pull-right hidden-xs">
          <li class="active">Liste des utilisateurs</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <!-- ALERT ERREUR OU SUCCESS POUR CREATION / MODIFICATION / SUPPRESSION / -->
  <?php if (isset($success) || isset($error)) {?>
    <div class="row">
      <div class="alert fade in alert-<?php echo isset($success) ? 'success' : 'danger'; ?>">
        <a class="close" data-dismiss="alert">×</a>
        <strong><?php echo isset($success) ? $success : $error; ?></strong>
      </div>
    </div>
  <?php } ?>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <?php if (isset($users) && $users != false) {?>
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Email</th>
              <th>Nom</th>
              <th>Prénom</th>
              <th>Type de compte</th>
              <th>Date d'inscription</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($users as $user) { ?>
              <tr <?php echo get_style_account($user['fb_account']); ?>>
                <td><?= $user['id'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['firstname'] ?></td>
                <td><?= $user['lastname'] ?></td>
                <td class="text-center"><?php echo get_icon_account($user['fb_account']); ?></td>
                <td><?php echo date("d/m/Y", strtotime($user['created_at'])); ?></td>
                <td class="text-center">
                  <?php if ($user['fb_account'] == '0') { # si c'est une adresse FB on ne peut pas modifier ?>
                    <a href="<?php echo site_url('users/edit?id='.$user['id'].'&amp;email='.$user['email']); ?>" class="btn btn-warning">Editer</a>
                  <?php } else { ?>
                    <a href="#" class="btn btn-warning" disabled="">Editer</a>
                  <?php } ?>
                  <a href="<?php echo site_url('users/destroy?id='.$user['id'].'&amp;email='.$user['email']); ?>" class="btn btn-danger">Supprimer</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } else {?>
        <p class="text-center"><strong>Aucun utilisateur enregistré pour le moment.</strong></p>
      <?php } ?>
    </div>
  </div>
</div>
