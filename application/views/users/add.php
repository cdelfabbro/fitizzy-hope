<!-- Topic Header -->
<div class="topic">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h3 class="primary-font">Fitizzy-hope</h3>
      </div>
      <div class="col-sm-8">
        <ol class="breadcrumb pull-right hidden-xs">
          <li><a href="<?php echo site_url("");?>">Liste des utilisateurs</a></li>
          <li class="active">Créer un utilisateur</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="sign-form">
        <h3>Créez un utilisateur par email</h3>
        <hr>
        <form role="form" method="post" action="<?php echo site_url('users/create_by_email'); ?>">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <h5>Votre compte</h5>
              <div class="form-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="Adresse email" value="<?php echo set_value('email'); ?>" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez une adresse email valide ici." data-original-title="Adresse email">
                <span class="text-danger"><?php echo form_error('email'); ?></span>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <input type="password" class="form-control margin-bottom-xs" id="password" name="password" placeholder="Mot de passe" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Cinq caractères minimum." data-original-title="Mot de passe">
                    <span class="text-danger"><?php echo form_error('password'); ?></span>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control" id="repeat-password" name="repeat-password" placeholder="Répétez le mot de passe" required="required" data-toggle="popover" data-trigger="focus" data-content="Être sûr de toujours s'en rappeler." data-original-title="Répétez le mot de passe">
                    <span class="text-danger"><?php echo form_error('repeat-password'); ?></span>
                  </div>
                </div>
              </div>
              <hr>

              <h5>Votre profil</h5>
              <div class="form-group">
                <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" value="<?php echo set_value('prenom'); ?>" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez votre prénom ici." data-original-title="Prénom">
                <span class="text-danger"><?php echo form_error('prenom'); ?></span>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" value="<?php echo set_value('nom'); ?>" required="required" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Entrez votre nom ici." data-original-title="Nom">
                <span class="text-danger"><?php echo form_error('nom'); ?></span>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-primary">Créer un compte par email</button><hr>
                <span class="error text-danger"><?php if (isset($error)) { echo $error; } ?></span>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <h3>Créez un utilisateur par Facebook</h3><hr>
      <div class="text-center text-facebook">
        <a href="<?= $login_url_fb ?>" class="btn btn-primary btn-lg"><i class="fa fa-facebook"></i> - Créer un compte par facebook</a><br>
        <span class="error text-danger"><?php if (isset($error_fb)) { echo $error_fb; } ?></span>
      </div>
    </div>
  </div> <!-- / .row -->
</div> <!-- / container -->
