    </div>
    <div id="footer"><hr>
      <div class="container">
        <p class="text-center">Site réalisé par Christophe Del Fabbro -
          <a href="https://bitbucket.org/cdelfabbro/fitizzy-hope">
            <i class="fa fa-bitbucket"></i> Bitbucket - fitizzy-hope
          </a>
        </p>
      </div>
    </div>
  <script src="<?php echo assets_url();?>javascript/jquery_1.11.1.min.js"></script>
  <script src="<?php echo assets_url();?>javascript/bootstrap.min.js"></script>
  <script src="<?php echo assets_url();?>javascript/customs.js"></script>
  </body>
</html>
