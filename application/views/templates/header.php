<!doctype html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Projet test - Fitizzy">
    <meta name="author" content="Christophe Del Fabbro">
    <link rel="icon" href="<?php echo assets_url();?>images/favicon.ico">

    <title>Fitizzy-Hope</title>

    <!-- Flatly Bootstrap core CSS -->
    <link href="<?php echo assets_url();?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Resources -->
    <link href="<?php echo assets_url();?>fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Customs -->
    <link href="<?php echo assets_url();?>css/customs.css" rel="stylesheet">
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li><a class="navbar-brand" href="<?php echo site_url("");?>">Liste des utilisateurs</a></li>
            <li><a class="navbar-brand" href="<?php echo site_url("users/add");?>">Créer un utilisateur</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <?php if ($this->session->userdata('user_id')) { # user connecté ?>
              <li><a class="navbar-brand" href="<?php echo site_url("users/show");?>">Voir mon profil</a></li>
              <li><a class="navbar-brand" href="<?php echo site_url("connections/destroy?id=".$this->session->userdata('user_id')."&amp;email=".$this->session->userdata('user_email'));?>">Se déconnecter</a></li>
            <?php } else { ?>
              <li><a class="navbar-brand pull-right" href="<?php echo site_url("connections");?>">Se connecter</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>

    <div class="container wrapper">
