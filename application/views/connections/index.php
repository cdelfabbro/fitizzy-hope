<!-- Topic Header -->
<div class="topic">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h3 class="primary-font">Fitizzy-hope</h3>
      </div>
      <div class="col-sm-8">
        <ol class="breadcrumb pull-right hidden-xs">
          <li><a href="<?php echo site_url("");?>">Liste des utilisateurs</a></li>
          <li class="active">Se connecter</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-6 col-sm-6">
      <div class="sign-form">
        <h3>Connectez-vous par email</h3>
        <hr>
        <form role="form" class="col-md-10 col-md-offset-1" method="post" action="<?php echo site_url('connections/create_by_email'); ?>">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
              <input type="email" class="form-control" id="email" name="email" value="<?php if (isset($email)) { echo $email; } ?>" placeholder="Entrez votre email" required="">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe" required="">
            </div>
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Connexion par Email</button><br>
            <span class="connexion-error text-danger"><?php if (isset($error)) { echo $error; } ?></span>
            <p class="no-account">Pas encore inscrit ? <a href="<?php echo site_url('users/add'); ?>">Créez un compte utilisateur par Email.</a></p>
          </div>
          <hr>
        </form>
      </div>
    </div>
    <div class="col-md-6 col-sm-6">
      <div class="sign-form">
        <h3>Connectez-vous par Facebook</h3><hr>
        <div class="text-center text-facebook-conex">
          <a href="<?= $login_url_fb ?>" class="btn btn-primary btn-lg"><i class="fa fa-facebook"></i> - Connexion par Facebook</a><br>
          <span class="error text-danger"><?php if (isset($error_fb)) { echo $error_fb; } ?></span>
          <p class="no-account">Pas encore inscrit ? <a href="<?php echo site_url('users/add'); ?>">Créez un compte utilisateur par Facebook.</a></p>
        </div>
      </div>
    </div>
  </div> <!-- / .row -->
</div> <!-- / .container -->
