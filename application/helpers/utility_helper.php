<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  # retourne l'url du répertoire assets
  function assets_url() {
    return base_url().'assets/';
  }

  # retourne le style appliqué en fonction du type de compte
  function get_style_account($fb_account) {
    if ($fb_account == '1')
      return " class='info'";
    else
      return " class='active'";
  }

  # retourne l'icone appliqué en fonction du type de compte
  function get_icon_account($fb_account) {
    if ($fb_account == '1')
      return " <i class='fa fa-facebook'></i>";
    else
      return " <i class='fa fa-envelope'></i>";
  }
?>
