<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

  public function User_model()
  {
    parent::__construct();
  }

  public function get_all()
  {
    # récupération de tous les users
    $users = $this->db->get('users');
    # SELECT * FROM users

    # Si il ya des users on les retournent
    if ($users->num_rows >= 1)
      return $users->result_array();
    else # aucun user
      return false;
  }

  public function get_user_by_id($user_id)
  {
    # récupération de la première ligne qui correspond à l'id saisie
    $user = $this->db->get_where('users', array('id' => $user_id), 1);
    # SELECT * FROM users WHERE id = XXX LIMIT = 1

    # si on a bien une ligne qui correspond
    if ($user->num_rows == 1)
      return $user->row_array();
    else
      return false;
  }

  public function get_user_by_id_and_email($user_id, $user_email)
  {
    # récupération de la première ligne qui correspond à l'id saisie
    $user = $this->db->get_where('users', array('id' => $user_id, 'email' => $user_email, 'fb_account' => '0'), 1);
    # SELECT * FROM users WHERE id = XXX LIMIT = 1

    # si on a bien une ligne qui correspond
    if ($user->num_rows == 1)
      return $user->row_array();
    else
      return false;
  }

  public function get_user_by_email_and_password($email, $password)
  {
    # récupération de la première ligne qui correspond à l'email saisie
    $user = $this->db->get_where('users', array('email' => $email, 'fb_account' => '0'), 1);
    # SELECT * FROM users WHERE email = 'XXXXX' LIMIT = 1

    # si on a bien une ligne qui correspond
    if ($user->num_rows == 1)
    {
      # on vérifie si le mot de passe correspond aussi
      if ($user->row()->password == md5($password))
        return $user->row_array();
      else
        return false;
    }
    else
      return false;
  }

  public function get_user_by_email_fb($email)
  {
    # récupération de la première ligne qui correspond à l'email saisie
    $user = $this->db->get_where('users', array('email' => $email, 'fb_account' => '1'), 1);
    # SELECT * FROM users WHERE email = 'XXXXX' LIMIT = 1

    # si on a bien une ligne qui correspond
    if ($user->num_rows == 1)
      return $user->row_array();
    else
      return false;
  }

  public function set_user_by_email($infos)
  {
    $user = array(
      'email' => $infos['email'],
      'password' => md5($infos['password']),
      'firstname' => $infos['prenom'],
      'lastname' => $infos['nom'],
      'fb_account' => false
    );

    //insert into user ...
    $this->db->insert('users', $user); // on insére un nouveau user

    return $this->db->affected_rows(); # retourne le nombre de ligne insérée
  }

  public function set_user_by_fb($infos)
  {
    $user = array(
      'email' => $infos['email'],
      'firstname' => $infos['first_name'],
      'lastname' => $infos['last_name'],
      'fb_account' => true
    );

    //insert into user ...
    $this->db->insert('users', $user); // on insére un nouveau user

    return $this->db->affected_rows(); # retourne le nombre de ligne insérée
  }

  public function update_user($infos, $update_password = null)
  {
    $user = array(
      'email' => $infos['email'],
      'firstname' => $infos['prenom'],
      'lastname' => $infos['nom']
    );

    if ($update_password == 1) # le mot de passe doit aussi être modifié
      $user['password'] = md5($infos['password']);

    // # UPDATE users SET XXX = 'XXX', XXX = 'XXX' WHERE id = XX
    $this->db->where('id', $infos['id'])->update('users', $user);

    return $this->db->affected_rows(); # retourne le nombre de ligne modifiée
  }

  public function destroy_user($user_id, $user_email)
  {
    $this->db->delete('users', array('id' => $user_id, 'email' => $user_email));
    # DELETE FROM users WHERE id = XXX
    return $this->db->affected_rows(); # nombre de ligne supprimé
  }

  public function count_user_by_email($email)
  {
    # récupération de la première ligne qui correspond à l'email saisie
    $user = $this->db->get_where('users', array('email' => $email, 'fb_account' => '0'), 1);
    # SELECT * FROM users WHERE email = 'XXXXX' LIMIT = 1

    # retourne 1 ou 0 (nb de lignes)
    return $user->num_rows;
  }

  public function count_user_by_email_fb($email)
  {
    # récupération de la première ligne qui correspond à l'email saisie
    $user = $this->db->get_where('users', array('email' => $email, 'fb_account' => '1'), 1);
    # SELECT * FROM users WHERE email = 'XXXXX' AND fb_account = X LIMIT = 1

    # retourne 1 ou 0 (nb de lignes)
    return $user->num_rows;
  }

  public function user_test_password($id, $password)
  {
    # récupération de la première ligne qui correspond à l'id saisie
    $user = $this->db->get_where('users', array('id' => $id), 1);
    # SELECT * FROM users WHERE id = XXX LIMIT = 1

    # si on a bien une ligne qui correspond
    if ($user->num_rows == 1)
    {
      # on vérifie si le mot de passe correspond aussi
      if ($user->row()->password == md5($password))
        return 1;
      else
        return 0;
    }
    else
      return -1;
  }

  public function user_is_fb_account($id) {
    # récupération de la première ligne qui correspond à l'id saisie
    $user = $this->db->get_where('users', array('id' => $id), 1);
    # SELECT * FROM users WHERE id = XXX LIMIT = 1

    # si on a bien une ligne qui correspond
    if ($user->num_rows == 1)
    {
      if ($user->row()->fb_account == '1')
        return 1; # user facebook
      else
        return 0; # user email
    }
    else # erreur id ou connexion BDD
      return -1;
  }
}
