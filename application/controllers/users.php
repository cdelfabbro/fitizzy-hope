<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function Users()
	{
		parent::__construct();

    # chargement du model
    $this->load->model('user_model');
    # Récupère automatiquement api_key et secret dans application/config/facebook.php
    $this->load->library('facebook');
	}

	# GET /users
	public function index($data = null)
	{
   	# Récupère tous les users
    $data['users'] = $this->user_model->get_all();

  	$this->load->template('users/index', $data);
	}

	# GET /users/show
	public function show()
	{
    $user_id = $this->session->userdata('user_id'); # récupère l'id de l'user en session

    # récupère les infos de l'user (récupération en base pour être à jour en cas de mofication des données pendant une session)
    $data['user'] = $this->user_model->get_user_by_id($user_id);

    if ($data['user'] == false) # problème - id n'existe sûrement pas
    {
      $data['error'] = 'Une erreur s\'est produite en voulant consulter votre profil, veuillez recommencer ultérieurement.';
      $this->index($data);
    }
    else
      $this->load->template('users/show', $data);
	}

	# GET /users/add
	public function add()
	{
    $data['login_url_fb'] = $this->login_url_facebook();
		$this->load->template('users/add', $data);
	}


	# POST /users/create_by_email
	public function create_by_email()
	{
		# définition des champs avec leurs restrictions
	  $this->form_validation->set_rules('email', '"Email"', 'trim|required|valid_email|min_length[5]|max_length[50]|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('password','"Mot de passe"', 'trim|required|min_length[5]|max_length[50]|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('repeat-password','"Répétez le Mot de passe"', 'trim|required|min_length[5]|max_length[50]|matches[password]|encode_php_tags|xss_clean');

	  $this->form_validation->set_rules('prenom', '"Prénom"', 'trim|required|min_length[3]|max_length[50]|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('nom','"Nom"', 'trim|required|min_length[3]|max_length[50]|encode_php_tags|xss_clean');

    # Création du lien login FB au cas ou mauvaise inscription
    $data['login_url_fb'] = $this->login_url_facebook();

  	# si le formulaire répond bien aux restrictions
  	if ($this->form_validation->run())
  	{
  		$inputs = $this->input->post(NULL, TRUE); // retourne tous les POST dans un tableau(key, value) + XSS filtre

  		# Je vérifie que l'email n'existe pas déjà pour un user EMAIL
  		if ($this->user_model->count_user_by_email($inputs['email']) == 0)
  		{
	  		# j'appelle la méthode de mon modèle pour ajouter un nouveau user
	  		$affected_rows = $this->user_model->set_user_by_email($inputs);

	  		if ($affected_rows != 1) { # une erreur s'est produite
	  			$data['error'] = 'Une erreur s\'est produite durant l\'ajout de l\'utilisateur par Email, veuillez recommencer.';
	  			$this->load->template('users/add', $data);
	  		}
	  		else { # ajout réussi
	  			$data['success'] = 'L\'utilisateur par Email a bien été ajouté.';
	  			$this->index($data); # redirection vers la liste utilisateur (accueil)
	  		}
	  	}
	  	else {
	  		$data['error'] = 'Votre adresse email est déjà utilisée.';
	  		$this->load->template('users/add', $data);
	  	}
  	}
  	else
  		$this->load->template('users/add', $data);
	}


  # POST /users/create_by_facebook
  public function create_by_facebook()
  {
    $user = $this->facebook->getUser(); # récupère les infos user par Facebook
    # Création du lien login FB
    $data['login_url_fb'] = $this->login_url_facebook();

    if ($user) { # si on a bien récupérer les infos
      try {
        $user_profile = $this->facebook->api('/me'); # on récupère les infos traitables
      }
      catch (FacebookApiException $e) {
        $user = null;
        $user_profile = null;
      }
    }
    else {
      $data['error_fb'] = 'Une erreur s\'est produite durant l\'ajout de l\'utilisateur Facebook, veuillez recommencer.';

      $this->facebook->destroySession(); # on détruit la session avec Facebook
      $this->load->template('users/add', $data);
    }

    # détruit la session avec Facebook une fois utilisée
    $this->facebook->destroySession();

    if ($user && isset($user_profile)) { # si on a bien récupéré les infos du profils
      # Je vérifie que l'email n'existe pas déjà pour un utilisateur FB
      if ($this->user_model->count_user_by_email_fb($user_profile['email']) == 0)
      {
        # j'appelle la méthode de mon modèle pour ajouter un nouveau user
        $affected_rows = $this->user_model->set_user_by_fb($user_profile);

        if ($affected_rows != 1) { # une erreur s'est produite
          $data['error_fb'] = 'Une erreur s\'est produite durant l\'ajout de l\'utilisateur Facebook, veuillez recommencer.';
          $this->load->template('users/add', $data);
        }
        else { # ajout réussi
          $data['success'] = 'L\'utilisateur Facebook a bien été ajouté.';
          $this->index($data); # redirection vers la liste utilisateur (accueil)
        }
      }
      else {
        $data['error_fb'] = 'Votre compte facebook est déjà inscrit.';
        $this->load->template('users/add', $data);
      }
    }
  }

	# GET /users/edit/:id
	public function edit()
	{
    $user_id = $this->input->get('id', true); # récupère l'id de l'user en GET
    $user_email = $this->input->get('email', true); # récupère l'email de l'user en GET

    $user = $this->user_model->get_user_by_id_and_email($user_id, $user_email); # récupère les infos de l'user

    if ($user == false)
    {
      $data['error'] = 'Une erreur s\'est produite durant la demande de modification de l\'utilisateur, veuillez recommencer.';
      $this->index($data);
    }
    else
      $this->load->template('users/edit', array('user' => $user));
	}

	# POST /users/update
	public function update()
	{
    # définition des champs avec leurs restrictions
    $this->form_validation->set_rules('email', '"Email"', 'trim|required|valid_email|min_length[5]|max_length[50]|encode_php_tags|xss_clean');
    $this->form_validation->set_rules('prenom', '"Prénom"', 'trim|required|min_length[3]|max_length[50]|encode_php_tags|xss_clean');
    $this->form_validation->set_rules('nom','"Nom"', 'trim|required|min_length[3]|max_length[50]|encode_php_tags|xss_clean');

    # si la demande de modification de password a été faite
    if ($this->input->post('editPassword', true) == "checked")
    {
      $this->form_validation->set_rules('old-password','"Ancien mot de passe"', 'trim|required|min_length[5]|max_length[50]|encode_php_tags|xss_clean');
      $this->form_validation->set_rules('password','"Nouveau mot de passe"', 'trim|required|min_length[5]|max_length[50]|encode_php_tags|xss_clean');
      $this->form_validation->set_rules('repeat-password','"Répétez le Mot de passe"', 'trim|required|min_length[5]|max_length[50]|matches[password]|encode_php_tags|xss_clean');
    }

    # si le formulaire répond bien aux restrictions
    if ($this->form_validation->run())
    {
      $inputs = $this->input->post(NULL, TRUE); // retourne tous les POST dans un tableau(key, value) + XSS filtre
      $test_password = null;
      $fb_account = $this->user_model->user_is_fb_account($inputs['id']); # test si l'user est un user FB

      if ($fb_account == 1) { # l'use s'est inscrit par FB: modification interdite
        $data['error'] = 'La modification d\'un utilisateur Facebook est interdite';
        $this->index($data); # redirection vers la liste utilisateur (accueil)
      }
      elseif ($fb_account == -1)
      {
        $data['error'] = 'Une erreur est survenue, aucune information n\'a été modifié';
        $this->index($data); # redirection vers la liste utilisateur (accueil)
      }
      else { # l'user s'est inscrit pas email: il peut être modifié
        # si la demande de modification de password a été faite
        if ($this->input->post('editPassword', true) == "checked")
        {
          $test_password = $this->user_model->user_test_password($inputs['id'], $inputs['old-password']); # test si l'ancien mot de passe est bon

          if ($test_password == 0) # mauvais password
          {
            $data['error'] = 'Votre mot de passe actuel ne correspond pas à votre saisie. Aucune information n\'a été modifiée';
            $data['user'] = $this->user_model->get_user_by_id($this->input->post('id', true)); # récupère les infos de l'user pour recharger les inputs
            $this->load->template('users/edit', $data);
            return;
          }
          else if ($test_password != 1) # une erreur s'est produite
          {
            $data['error'] = 'Une erreur est survenue, aucune information n\'a été modifié';
            $data['user'] = $this->user_model->get_user_by_id($this->input->post('id', true)); # récupère les infos de l'user pour recharger les inputs
            $this->load->template('users/edit', $data);
            return;
          }
        }
        $affected_rows = $this->user_model->update_user($inputs, $test_password); # met à jour l'user et renvoit le nb de lignes affectées

        if ($affected_rows == 1) { # modification réussie
          $data['success'] = 'L\'utilisateur a bien été mofidié.';
          $this->index($data); # redirection vers la liste utilisateur (accueil)
        }
        else if ($affected_rows == 0) { # aucune information à mettre à jour
          $data['user'] = $this->user_model->get_user_by_id($this->input->post('id', true)); # récupère les infos de l'user pour recharger les inputs
          $data['error'] = 'Aucune modification n\'a été demandée.';
          $this->load->template('users/edit', $data);
        }
        else { # une erreur s'est produite
          $data['user'] = $this->user_model->get_user_by_id($this->input->post('id', true)); # récupère les infos de l'user pour recharger les inputs
          $data['error'] = 'Une erreur s\'est produite durant la modification de l\'utilisateur, veuillez recommencer.';
          $this->load->template('users/edit', $data);
        }
      }
    }
    else # le formulaire n'est pas conforme
    {
      $data['error'] = 'Le formulaire n\'est pas conforme. Aucune modification n\'a été modifiée';
      $data['user'] = $this->user_model->get_user_by_id($this->input->post('id', true)); # récupère les infos de l'user pour recharger les inputs
      $this->load->template('users/edit', $data);
    }
	}

	# GET /users/destroy
	public function destroy()
	{
    $user_id = $this->input->get('id', true); # récupère l'id de l'user en GET
    $user_email = $this->input->get('email', true); # récupère l'email de l'user en GET

    $affected_row = $this->user_model->destroy_user($user_id, $user_email); # delete l'user et renvoi le nb de ligne affectée

    if ($affected_row == 0) # l'user n'a pas été supprimé
      $data['error'] = 'Une erreur est survenue, l\'utilisateur n\'a pas pu être supprimé.';
    else {
      $data['success'] = 'L\'utilisateur a été correctement supprimée.';

      # si destroy de l'user actuellement en session
      if ($user_id == $this->session->userdata('user_id') && $user_email == $this->session->userdata('user_email')) {
        # Détruit la session serveur
        $this->session->sess_destroy();
        # on détruit la session avec Facebook
        $this->facebook->destroySession();
      }
    }

    $this->index($data); # redirection vers la liste utilisateur (accueil)
	}

  private function login_url_facebook() {
    return $this->facebook->getLoginUrl(array(
      'redirect_uri' => site_url('users/create_by_facebook'), // ou redirige facebook après authentification
      'scope' => array("email") // permissions
    ));
  }
}
