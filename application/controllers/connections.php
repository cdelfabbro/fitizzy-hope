<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Connections extends CI_Controller {

  public function Connections()
  {
    parent::__construct();

    # chargement du model
    $this->load->model('user_model');
    # Récupère automatiquement api_key et secret dans application/config/facebook.php
    $this->load->library('facebook');
  }

  public function index()
  {
    $data['login_url_fb'] = $this->login_url_facebook();
    $this->load->template('connections/index', $data);
  }

  public function create_by_email()
  {
    # définition des champs avec leurs restrictions
    $this->form_validation->set_rules('email', '"Email"', 'trim|required|valid_email|min_length[5]|max_length[50]|encode_php_tags|xss_clean');
    $this->form_validation->set_rules('password','"Mot de passe"', 'trim|required|min_length[5]|max_length[50]|encode_php_tags|xss_clean');

    # Création du lien login FB
    $data['login_url_fb'] = $this->login_url_facebook();

    # si le formulaire répond bien aux restrictions
    if ($this->form_validation->run()) {
      $inputs = $this->input->post(NULL, TRUE); // retourne tous les POST dans un tableau(key, value) + XSS filtre

      # j'appelle la méthode de mon modèle qui vérifie si l'user' existe et le mot de passe correspond
      $user = $this->user_model->get_user_by_email_and_password($inputs['email'], $inputs['password']);

      if ($user == false) { # user saisi n'existe pas
        $data['email'] = $inputs['email'];
        $data['error'] = 'Identifiants incorrects';
        $this->load->template('connections/index', $data);
      }
      else { # user existe
        # ajout de ses informations en session
        $user = array(
          'user_id' => $user['id'],
          'user_email' => $user['email'],
          'user_firstname' => $user['firstname'],
          'user_lastname' => $user['lastname']
        );
        $this->session->set_userdata($user);
        redirect('users/show');
      }
    }
    else { # le formulaire n'est pas valide
      $data['error'] = 'Le formulaire n\'est pas conforme.';
      $this->load->template('connections/index', $data);
    }
  }

  public function create_by_facebook()
  {
    $user = $this->facebook->getUser(); # récupère les infos user par Facebook
    # Création du lien login FB
    $data['login_url_fb'] = $this->login_url_facebook();

    if ($user) { # si on a bien récupérer les infos
      try {
        $user_profile = $this->facebook->api('/me'); # on récupère les infos traitables
      }
      catch (FacebookApiException $e) {
        $user = null;
        $user_profile = null;
      }
    }
    else {
      $data['error_fb'] = 'Une erreur s\'est produite durant la connexion de l\'utilisateur Facebook, veuillez recommencer.';

      $this->facebook->destroySession(); # on détruit la session avec Facebook
      $this->load->template('connections/index', $data);
    }

    if ($user && isset($user_profile)) { # si on a bien récupéré les infos du profils
      # j'appelle la méthode de mon modèle qui vérifie si l'user' existe et c'est bien un compte FB
      $user_fb = $this->user_model->get_user_by_email_fb($user_profile['email']);

      if ($user_fb == false) { # user saisi n'existe pas
        $data['error_fb'] = 'Inscrivez-vous avec Facebook avant de vous connecter';
        $this->load->template('connections/index', $data);
      }
      else { # user existe
         # ajout de ses informations en session
        $user_fb = array(
          'user_id' => $user_fb['id'],
          'user_id_fb' => $user_profile['id'],
          'user_email' => $user_profile['email'],
          'user_firstname' => $user_profile['first_name'],
          'user_lastname' => $user_profile['last_name'],
          'user_link' => $user_profile['link']
        );
        $this->session->set_userdata($user_fb);
        redirect('users/show');
      }
    }
    else {
      $data['error_fb'] = 'Votre compte facebook est déjà inscrit.';
      $this->load->template('users/add', $data);
    }
  }

  public function destroy()
  {
    # Détruit la session
    $this->session->sess_destroy();
    # on détruit la session avec Facebook (si il yen a une)
    $this->facebook->destroySession();
    # Redirige vers la page d'accueil
    redirect('');
  }

  private function login_url_facebook() {
    return $this->facebook->getLoginUrl(array(
      'redirect_uri' => site_url('connections/create_by_facebook'), // ou redirige facebook après authentification
      'scope' => array("email") // permissions
    ));
  }
}
