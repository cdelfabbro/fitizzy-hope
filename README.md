# Fitizzy-hope #

### Sujet ###

Création d'un annuaire d'utilisateurs.

Le projet doit être développé en PHP5 avec une base de données MySQL.
L'interface doit permettre de: créer/modifier/lister/supprimer des utilisateurs.

On suppose que les utilisateurs une fois créés peuvent se connecter à un site (ce dernier n'est pas à  développer).

Facultatif: Création de son propre profil via Facebook.

### Technologies utilisées ###

* [HTML5](http://www.w3schools.com/html/html5_intro.asp)
* [PHP5](http://php.net/)
* [CodeIgniter 2.1.4](https://ellislab.com/codeigniter)
* [Bootstrap 3.2](http://getbootstrap.com/)
* [Thème Flatly](http://bootswatch.com/flatly/)
* [Base de données MYSQL](http://www.mysql.fr/)
* [Facebook SDK for PHP](https://developers.facebook.com/docs/reference/php/4.0.0)

### Installation ###

* Télécharger les sources
```
git clone https://bitbucket.org/cdelfabbro/fitizzy-hope.git
```

* Créer une base dans votre base de donnée MYSQL

* Créer les tables users et ci_sessions dans cette base
```
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fb_account` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;
```

* Créer une application sur [Facebook Developers](http://www.facebook.com/developers/)

* Ajouter la Plateforme "Website" à cette application et y renseigner l'URL de votre application

* Configurer le framework CodeIgniter

* Renseigner l'URL de votre application - fichier /application/config/config.php
```
$config['base_url'] = "VOTRE WEBHOST";
```

* Renseigner les informations de votre base de donnée - fichier /application/config/database.php
```
$db['default']['hostname'] = 'VOTRE HOSTNAME';
$db['default']['username'] = 'VOTRE USER';
$db['default']['password'] = 'VOTRE MOT DE PASSE';
$db['default']['database'] = 'VOTRE BASE DE DONNEES';
$db['default']['dbdriver'] = 'mysql';
```

* Renseigner la KEY_API et KEY_SECRET Facebook - fichier /application/config/facebook.php
```
$config['appId']   = 'APP ID FB';
$config['secret']  = 'SECRET KEY';
```

Enjoy !

### Inspiration ###

* [Punnetkay - Facebook-PHP-Codeigniter](https://github.com/puneetkay/Facebook-PHP-CodeIgniter)

### Auteur ###

Christophe Del Fabbro