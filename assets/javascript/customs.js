$(function(){
  $('#email').popover();
});

$(function(){
  $('#password').popover();
});

$(function(){
  $('#repeat-password').popover();
});

$(function(){
  $('#old-password').popover();
});

$(function(){
  $('#prenom').popover();
});

$(function(){
  $('#nom').popover();
});

$(function(){
  $('#editPassword').click(function() {
    if ($(this).is(':checked'))
      $('#old-password, #password, #repeat-password').removeAttr('disabled');
    else
      $('#old-password, #password, #repeat-password').attr('disabled', 'true');
  });
});
